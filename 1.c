#include <stdio.h>
int main() {
 float num;
 printf("Please enter a number: ");
 scanf("%f", &num);

 if (num == 0)
    printf("You entered 0(zero).");
 else if (num < 0)
    printf("You entered a negative number.");
 else
    printf("You entered a positive number.");
 return 0;
}
