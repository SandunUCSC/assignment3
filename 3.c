
#include <ctype.h>
#include <stdio.h>
int main() {
   char c;
   char lv, uv;
   printf("Please enter an alphabet: ");
   scanf("%c", &c);

   lv = (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');

   uv = (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U');
if (!isalpha(c))
      printf("This is a non-alphabetic character.");
   else if (lv || uv)
      printf("This is a vowel.");
   else
      printf("This is a consonant.");
return 0;
}
